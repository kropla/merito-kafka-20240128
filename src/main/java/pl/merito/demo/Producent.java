package pl.merito.demo;

import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

public class Producent {
    public static final String TOPIC_NAME = "topic_2";
    private static final Logger log = LoggerFactory.getLogger(Producent.class);

    public static void main(String[] args) {
        log.info("Hello from Kafka Producer");

        String bootstrapServers = "127.0.0.1:9092";

        Properties properties = new Properties();
        properties.setProperty("bootstrap.servers", bootstrapServers);
        properties.setProperty("key.serializer", StringSerializer.class.getName());
        properties.setProperty("value.serializer", StringSerializer.class.getName());

        KafkaProducer<String, String> producer = new KafkaProducer<>(properties);

        ProducerRecord<String, String> producerRecord = new ProducerRecord<>(TOPIC_NAME, "hello kafka 123 saddasdasdsadds");
        producer.send(producerRecord, new Callback() {
            @Override
            public void onCompletion(RecordMetadata metadata, Exception e) {
                if (e == null) {
                    log.info("Received new metadata \n"
                                 + "Topic: " + metadata.topic() + "\n"
                                 + "Partition: " + metadata.partition() + "\n"
                                 + "Offset: " + metadata.offset() + "\n"
                                 + "Timestamp: " + metadata.timestamp());
                } else {
                    log.error("Error while producing", e);
                }
            }
        });

        producer.flush();
        producer.close();

        log.info("Kafka Producer finished");
    }

}
