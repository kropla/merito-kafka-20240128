#### Create docker folder

``mkdir docker``

#### Move to created docker folder
``cd docker``

#### Download docker-compose configuration file with all kafka related configuration
``wget -O kafka-docker-compose.yaml https://raw.githubusercontent.com/conduktor/kafka-stack-docker-compose/master/zk-single-kafka-single.yml``

#### Run downloaded script - docker should download dependencies and run it in the background
``docker-compose -f kafka-docker-compose.yaml up -d``

#### Check running docker processes 
``docker ps`` 