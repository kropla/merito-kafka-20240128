# Projekt demonstracyjny Kafka na zajęcia pokazowe 

1. [Instalacja Kafki](01_KAFKA_INSTALLATION.md)
2. [Konfiguracja Gradle](02_GRADLE_CONFIGURATION.md)
2. [Utworzenie i testowanie projektu](03_TEST_PROJECT.md)
2. [Implementacja producenta kafki](04_KAFKA_PRODUCER.md)
2. [Implementacja konsumenta kafki](05_KAFKA_CONSUMER.md)