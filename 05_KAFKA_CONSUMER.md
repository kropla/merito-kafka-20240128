
1. Create kafka consumer class `dev.merito.demo.Consumer`

```java
    import org.slf4j.Logger;
    import org.slf4j.LoggerFactory;

    public class Consumer {
        private static final Logger log = LoggerFactory.getLogger(Consumer.class);

        public static void main(String[] args) {
            log.info("I am a Kafka Producer");
        }
    }
```

2. Create consumer properties (To know about each property, visit the official site of Kafka - https://kafka.apache.org/documentation. Navigate to Kafka > Documentation > Configurations > Producer Configs))

The required properties that we must specify are shown below:

`bootstrap.servers`: It is a list of the port pairs which are used for establishing an initial connection to the Kafka cluster. We use the bootstrap servers for making an initial connection to the cluster. This server is present in the host:port, host:port,... form.

`key.deserializer`: It is a type of Serializer class of the key that is used to implement the org.apache.kafka.common.serialization.Deserializer interface.

`value.deserializer`: It is a type of Deserializer class which implements the org.apache.kafka.common.serialization.Deserializer interface.

`group.id`: It is a unique string that identifies the consumer of a consumer group.

`auto.offset.reset`: required when no initial offset is present or if the current offset does not exist anymore on the server. There are the following values used to reset the offset values:

        `earliest`: This offset variable automatically reset the value to its earliest offset.

        `latest`: This offset variable reset the offset value to its latest offset.

        `none`: If no previous offset is found for the previous group, it throws an exception to the consumer.

Add to the code:
```java

import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import java.util.Properties;

public class Consumer {

    public static void main(String[] args) {
        String bootstrapServers = "127.0.0.1:9092";
        String groupId = "my-fourth-application";
        String topic = "demo_merito_topic";

        // create consumer configs
        Properties properties = new Properties();
        properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        properties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.setProperty(ConsumerConfig.GROUP_ID_CONFIG, groupId);
        properties.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
    }

}
```

3. Create the Kafka consumer inside the `main` method
```java
        import org.apache.kafka.clients.consumer.KafkaConsumer;
        (...)
            KafkaConsumer<String, String> producer = new KafkaConsumer<>(properties);
```

4. Subscribe to the topic
```java
        consumer.subscribe(Arrays.asList(demo_merito_topic));
```

5. Poll for some data
```java
        while (true) {
            log.info("Polling data from kafka");
            ConsumerRecords<String, String> records = consumer.poll(Duration.ofSeconds(1));

            for (ConsumerRecord<String, String> record : records) {
                log.info("K: " + record.key() + ", V: " + record.value() + ", O: " + record.offset());
            }
        }
```

