
1. Create kafka producer class `dev.merito.demo.Producer`

```java
    import org.slf4j.Logger;
    import org.slf4j.LoggerFactory;

    public class Producer {
        private static final Logger log = LoggerFactory.getLogger(Producer.class);

        public static void main(String[] args) {
            log.info("I am a Kafka Producer");
        }
    }
```

2. Create producer properties (To know about each property, visit the official site of Kafka - https://kafka.apache.org/documentation. Navigate to Kafka > Documentation > Configurations > Producer Configs))

The required properties that we must specify are shown below:

`bootstrap.servers`: It is a list of the port pairs which are used for establishing an initial connection to the Kafka cluster. We use the bootstrap servers for making an initial connection to the cluster. This server is present in the host:port, host:port,... form.

`key.serializer`: It is a type of Serializer class of the key that is used to implement the org.apache.kafka.common.serialization.Serializer interface.

`value.serializer`: It is a type of Serializer class which implements the org.apache.kafka.common.serialization.Serializer interface.

Add to the code:
```java

import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import java.util.Properties;

public class Producer {

    public static void main(String[] args) {
        String bootstrapServers = "127.0.0.1:9092";

        // create Producer properties
        Properties properties = new Properties();
        properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        properties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
    }

}
```

3. Create the Kafka producer inside the `main` method
```java
        import org.apache.kafka.clients.producer.KafkaProducer;
        (...)
        KafkaProducer<String, String> producer = new KafkaProducer<>(properties);
```

4. Create producer record
```java
        import org.apache.kafka.clients.producer.ProducerRecord;
        (...)
        ProducerRecord<String, String> producerRecord = new ProducerRecord<>("demo_merito_topic", "hello world");
```

5. Send the data
```java
        producer.send(producerRecord);  // send data - asynchronous
        producer.flush(); // flush data - synchronous
        producer.close(); // flush and close producer
```
The `flush()` will force all the data that was in `send()` to be produced and `close()` stops the producer. 
If these functions are not executed, the data will never be sent to Kafka as the main Java thread will exit before the data are flushed.

6. Create topic:
```shell
 kafka-topics.sh --bootstrap-server localhost:9092 --topic demo_merito_topic --create --partitions 3 --replication-factor 1
```

7. Listen kafka topic:
```shell
kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic demo_merito_topic
```